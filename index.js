function printKata(kata) {
  return kata;
}

// console.log(printKata("Hello World"));

function timesTen(num) {
  return num * 10;
}

// console.log(timesTen(5));

function sambungKata(kata1, kata2) {
  return kata1 + " dan " + kata2;
}

// console.log(sambungKata("Hello", "World"));

function greetPeople(people) {
  return `Halo selamat datang, ${people}`;
}

// console.log(greetPeople("Hafizh"))

function bigNumber(num) {
  if (num > 5) {
    return "angka ini besar";
  } else if (num === 3) {
    return "angka ini 3";
  } else {
    return "angka ini kecil";
  }
}
// = assign nilai (angka = 1, nama = "Hafizh")
// == membandingkan nilai antara dua variable apakah mereka sama (angka1 = 1, variable angka2 = 1 angka1 == angka2 => true, angka1 = 1 ,angka2 = 2 angka1 == angka2 => false)
// === membandingkan nilai dan tipe antara dua variable apakah mereka sama (1 === 1 => true, 1 === "1" => false)
// != membandingkan nilai antara dua variable apakah mereka berbeda (1 !=2 => true, 1 != "1" => false)
// !== membandingkan nilai dan tipe antara dua variable apakah mereka berbeda (1 !== 2 => true, 1 !== "1" => true)
// console.log(bigNumber(2));

// console.log("hafizh" === "hafizh");

function littleNumber(num) {
  if (num < 8) {
    return "angka ini kecil";
  } else if ((num = 5)) {
    return "angka ini 5";
  } else {
    return "angka ini besar";
  }
}

// console.log(littleNumber(5));

const nama = "Hafizh";

function loopingNama(num) {
  for (let i = 1; i <= num; i++) {
    console.log(`${i}. ${nama}`);
  }
}

// loopingNama(5);

// > lebih dari (10>5)
// >= lebih dari sama dengan (10>5 , 10>=10)
// < kurang dari (1< 2)
// <= kurang dari sama dengan (1<2, 1<=1)

// console.log(10 > 5);
// console.log(10 >= 5, 10 >= 10);
// console.log(1 < 2);
// console.log(1 <= 2, 1 <= 1);

// "Hafizh" => string
// 1 => number
// true false => boolean

class Fruit {
  constructor(names, price, taste) {
    this.names = names;
    this.price = price;
    this.taste = taste;
  }
  get getNames() {
    return `nama buah ini adalah ${this.names}`;
  }
  get getPrice() {
    return `harga buah ini adalah ${this.price}`;
  }
  get getTaste() {
    return `rasa buah ini adalah ${this.taste} `;
  }
  set setNames(newName) {
    this.names = newName;
  }
  set setPrice(newPrice) {
    this.price = newPrice;
  }
  set setTaste(newTaste) {
    this.taste = newTaste;
  }
}
// const Manis = "manis"
// let Apple = new Fruit();
// console.log(Apple);
// Apple.setNames = "Apple";
// console.log(Apple);
// console.log(Apple.getNames);
// Apple.setPrice = 10000;
// console.log(Apple.getPrice);
// Apple.setTaste = Manis
// console.log(Apple.getTaste);

let Durian = new Fruit("Durian", 20000, "enak");
console.log(Durian.getNames);
console.log(Durian.getPrice);
console.log(Durian.getTaste);
