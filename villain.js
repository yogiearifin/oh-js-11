// Given an array called villain that has the name of JoJo's Bizzare Adventure's antagonist.
// Using loop method, print each of the villain altogether with their respective parts.
// example:
// Input -> Antagonist()
// Output ->
// Part 1 antagonist is Dio Brando
// Part 2 antagonist is Kars
// Part 3 antagonist is DIO
// Part 4 antagonist is Kira Yoshikage
// Part 5 antagonist is Diavolo
// Part 6 antagonist is Enrico Pucci
// Part 7 antagonist is Funny Valentine
// Part 8 antagonist is Akefu Satoru

const villain = [
  "Dio Brando",
  "Kars",
  "DIO",
  "Kira Yoshikage",
  "Diavolo",
  "Enrico Pucci",
  "Funny Valentine",
  "Akefu Satoru",
];

function Antagonist() {
  
}

Antagonist();
