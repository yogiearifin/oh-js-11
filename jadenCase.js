// Jaden Smith is notorious because he is always capitalize every tweets he writes
// make a function that can convert regular phrase into "Jaden Phrase"
// input : "I like food and math"
// output: "I Like Food And Math"

const jadenCase = (str) => {

};

console.log(jadenCase("test YOUR MIGHT"));

// do not edit code below
function Test(fun, result, preview) {
  console.log(fun === result, preview);
}

Test(
  jadenCase("this is sparta"),
  "This Is Sparta",
  jadenCase("this is sparta")
);
Test(
  jadenCase("you only live once"),
  "You Only Live Once",
  jadenCase("you only live once")
);
Test(
  jadenCase("glints academy batch 11 front end class"),
  "Glints Academy Batch 11 Front End Class",
  jadenCase("glints academy batch 11 front end class")
);
